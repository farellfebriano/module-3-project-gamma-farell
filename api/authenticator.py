import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from queries.accounts import AccountOut,AccountOutHasPass,AccountQuesries


class MyAuthenticator(Authenticator):
    async def get_account_data(
        self,
        username: str,
        accounts: AccountQuesries,
    ):
        # Use your repo to get the account based on the
        # username (which could be an email)

        # --important-- we need to create another function in the
        # query becuase this specific return
        # calling a query function
        return accounts.get(username)

    def get_account_getter(
        self,
        accounts: AccountQuesries = Depends(),
    ):
        # Return the accounts. That's it.
        return accounts

    def get_hashed_password(self, account: AccountOutHasPass):
        # Return the encrypted password value from your
        # account object
        return account.hashed_password

    def get_account_data_for_cookie(self, account: AccountOutHasPass):
        # Return the username and the data for the cookie.
        # You must return TWO values from this method.
        return account.username, AccountOut(**account.dict())


authenticator = MyAuthenticator(os.environ["SIGNING_KEY"])
