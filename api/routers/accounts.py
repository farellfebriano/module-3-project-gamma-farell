from fastapi import APIRouter,Request,Depends,Response
from queries.accounts import AccountIn,AccountOut,AccountQuesries
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from pydantic import BaseModel

router=APIRouter()
# since the this class inherit from the token token class it change the
# structure of the object
# {
#   "access_token": "string",
#   "token_type": "Bearer",
#   "account": {
#     "id": "string",
#     "username": "string"
#   }
# }
# according to the input and the output that is showed in the fast api,
# we can see that the AccountQuesries.create will consume the AccountIn and
# return the object that is stated above "AccountToken"
class AccountToken(Token):
    account:AccountOut

class AccountForm(BaseModel):
    username:str
    password:str


@router.post('/api/accounts',response_model=AccountToken)
# the account queries is not yet created, it is the obejct where we need to
# create a connection to the database, i just confuse what is the output for
# the queries
async def create_account(
    info:AccountIn,
    request:Request,
    response:Response,
    queries:AccountQuesries = Depends()
):
    # hashed_password --> Use this method to hash your pasword so
    # then later be varivied by the authentication mechanism used
    # by the authenticator
    # so this code below will return a string that will be hashed
    hashed_password=authenticator.hash_password(info.password)

    # so the AccountQuesries.create will accept one parameters that is hashed string
    account=queries.create(info=info,hashed_password=hashed_password)
    form=AccountForm(username=info.username,password=info.password)
    token= await authenticator.login(response,request,form,queries)
    print(token,'---------------token----------------')
    return AccountToken(account=account, **token.dict())
