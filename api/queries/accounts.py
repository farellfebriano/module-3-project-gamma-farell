from pydantic import BaseModel
from typing import List
from .pool import pool


#creating an account that will be
class AccountIn(BaseModel):
    username:str
    password:str

class AccountOut(BaseModel):
    id:str
    username:str

# the lass that will have the hass password with the id and the username
class AccountOutHasPass(BaseModel):
    id:str
    username:str
    hashed_password: str

class AccountQuesries():
    def create(self,info:AccountIn,hashed_password:str):
        with pool.connection() as conn:
            with conn.cursor() as db:
                # since we dont save the password that the user type,
                # i change it to the password that has been hasshed
                result=db.execute(
                    '''
                    INSERT INTO accounts
                    (username,hash_pasword)
                    VALUES(%s,%s)
                    RETURNING id;
                    ''',
                    [
                        info.username,
                        hashed_password
                    ]
                )
                content=result.fetchone()
                # i think i need to return the object account out
                #--not done --
                info_dic=info.dict()
                info_dic['id']=content[0]
                return info_dic

    #the function that is used as a returned in authentication.get_account_data
    #HOW IT WORKS --> the authenticator will hash plaint text password and look to see if it matches
    # what we have in the databas, so if its doense, then it means that user entered
    # an incorect password
    def get(self,username:str):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result=db.execute(
                    '''
                    SELECT id,username,hash_pasword FROM accounts
                    WHERE username=%s
                    ''',
                    [
                        username
                    ]
                )
                content=result.fetchone()
                content_dic={'id':0,"username":"NONE","hashed_password":'NONE'}
                # make the contet into a dictionary do that it can be inputed to the contentdic
                i=0
                for key in content_dic.keys():
                    content_dic[key]=content[i]
                    i+=1
        return AccountOutHasPass(**content_dic)
